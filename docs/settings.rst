File settings.py
~~~~~~~~~~~~~~~~


Il file `settings.py` definisce tutte le impostazioni essenziali di Django.
Può essere esteso con personalizzazioni caratteristiche dell'installazione
nel file `settings_local.py` le cui variabili sovrascriveranno quelle
contenute nel `settings.py`.


* ``EDUCATIONAL_AREAS``: questa variabile contiene le aree nelle quali sono
  suddivisi i corsi di aggiornamento

  .. code-block:: python

    # Elenco aree
    EDUCATIONAL_AREAS = [
      ("1","Innovazione didattica"),
      ("2","Valutazione delle competenze"),
      ("3","Orientamento"),
      ("4","Cittadinanza e coesione sociale")
    ]

* ``ALLOWED_MAILS``: è una lista che contiene i provider e-mail autorizzati
  all'accesso.

  .. code-block:: python

    # Indirizzi e-mail per il login
    ALLOWED_MAILS = [
      "@scuola.alto-adige.it",
      "@provincia.bz.it",
      "@fuss.bz.it"
    ]

* [post_office] ``DEFAULT_FROM_EMAIL``: questa variabile contiene il mittente di
  default per le email inviate da parte della piattaforma

  .. code-block:: python

    # Invio e-mail
    DEFAULT_FROM_EMAIL = "FUSS Corsi di aggiornamento <no-reply@fuss.bz.it>"

* [post_office] ``POST_OFFICE``: dizionario di configurazione "avanzata" di
  post_office. Il valore default_priority fa sì che le email vengano
  inviate istantaneamente. Ciò accelera le procedure che richiedono l'uso
  dell'e-mail

  .. code-block:: python

    POST_OFFICE = {
      "DEFAULT_PRIORITY": "now",
    }

* [django-sites] ``SITE_ID``: indica l'ID nel database del sito attualmente in uso
(default: example.com , modificare da admin)

  .. code-block:: python

    SITE_ID = 1

* [password_reset] ``PASSWORD_RESET_TOKEN_EXPIRES``: configura la durata del token
  generato per il recupero della password. Di default è impostato a 2 ore.

  .. code-block:: python

     PASSWORD_RESET_TOKEN_EXPIRES = 7200 # 2 hours
