Documentazione del Sistema di Gestione dei corsi di aggiornamento - Guida per il tecnico
========================================================================================

.. figure:: images/logo.png
      :width: 100%


Il presente manuale è una guida rivolta a chiunque volesse eseguire una sua copia del sistema di gestione dei corsi di aggiornamento.


.. toctree::
   :maxdepth: 2

   quickstart
   settings


Contribuisci
""""""""""""

Chiunque può contribuire a migliorare questa documentazione che è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_.

Autori
~~~~~~

* Piergiorgio Cemin
* Marco Marinello

I dettagli e lo storico dei contributi sono disponibili sul `repository
git <https://gitlab.com/fusslab/gpa-tech-guide>`_.


Licenze
"""""""

.. image:: https://img.shields.io/badge/code-GPLv3-blue.png
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.png
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA
