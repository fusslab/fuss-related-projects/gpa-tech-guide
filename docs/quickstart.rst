Quickstart
==========

Questa è intesa come procedura essenziale per poter avviare
il programma.


I passi di questa guida sono intesi su sistemi \*nix derivati da Debian e che
utilizzino apt come package manager.

Ambiente virtuale (opzionale)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se si vuole utilizzare un virtual environment per isolare l'installazione,
procedere con l'installazione di ``python3-virtualenv`` per avviare l'ambiente:

.. code-block:: console

  # apt install python3-virtualenv

Si può quindi avviare l'ambiente ed iniziare ad utilizzarlo

.. code-block:: console

  $ virtualenv --python=python3 venv
  $ . venv/bin/activate


Ottenere una copia del sorgente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Può essere scaricata direttamente dal repository o richiedendo copia dell'archivio
compresso via e-mail.

.. code-block:: console

  (venv)$ git clone https://work.fuss.bz.it/git/gestione-piano-aggiornamento


Installare le dipendenze
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

  (venv)$ cd gestione-piano-aggiornamento
  (venv)$ pip3 install -r requirements.txt

alcune librerie utilizzano libreria di sistema che devono essere installate
col package manager di sistema. Consultare la documentazione della singola
dipendenza in caso di errori.


Configurare le personalizzazioni
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si faccia riferimento alla sezione "settings.py"


Creare le migrazioni
~~~~~~~~~~~~~~~~~~~~

Le migrazioni sono le entità che Django utilizza per mantenere traccia della
struttura del database che sta utilizzando e quella disegnata dallo sviluppatore
nei modelli. Per ogni installazione è necessario generare le migrazioni dopo
aver collegato Django al database.

.. code-block:: console

   (venv)$ python3 manage.py makemigrations updates upmin certificates poll


Creare il database ed un utente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'username di tutti gli utenti che devono accedere alla piattaforma devono essere
indirizzi e-mail validi e che terminino con un dominio autorizzato
nel file settings.

.. code-block:: console

  (venv)$ python3 manage.py migrate
  (venv)$ python3 manage.py createsuperuser --username=root@fuss.bz.it --email=


Importare i template e-mail
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il sistema utilizza dei template di post-office per inviare le email
che devono essere preventivamente caricati dai template salvati nel repository.

.. code-block:: console

  (venv)$ python3 manage.py shell -c 'import scripts.importa_templatemail'
