Documentazione del Sistema di Gestione dei corsi di aggiornamento - Guida per il tecnico
==========================================

La documentazione è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_.


Licenze
-------

.. image:: https://img.shields.io/badge/code-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.svg
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA

.. image:: https://img.shields.io/badge/screenshots-CC0-ff69b4.svg
    :target: https://creativecommons.org/publicdomain/zero/1.0/
    :alt: Screenshots CC0
